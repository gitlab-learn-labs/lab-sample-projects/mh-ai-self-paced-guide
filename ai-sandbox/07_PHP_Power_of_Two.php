<!-- Link to test answer on LeetCode: https://leetcode.com/problems/power-of-two/?envType=featured-list -->

<!-- Power of Two Problem: We are given an integer n, return true if it is a power of two, false if it is not. Provide two different solutions for us to choose from. -->